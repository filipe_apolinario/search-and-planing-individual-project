;;;
;;; Copyright (C) Filipe Apolinário <f.apolinario30@gmail.com> <Instituto Superior Tecnico - 70571
;;;
;;; File: G012.lisp
;;; Created on: Sat March 21 19:34 2015

;15 PUZZLE - 4x4 square with 15 pieces.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;DISCLAIMER:
;Sorry in advance to non-Portuguese speakers reading this code. 
;This code was written Portuguese to be consistent with the supporting material 'procura.lisp'.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;CODE SUMMARY:
;code divided in 4 sections, IMPORT DE FICHEIROS AUXILIARES, DADOS AUXILIARES, ESTRUTURAS 
;and FUNCOES.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;DADOS AUXILIARES - global variables used in this file.

;ESTRUTURAS - Custom structures used in this file.

;FUNCOES - This section is declaration of functions in the project. 
;Also, this section is divided in X subsection, FUNCOES AUXILIARES SUPORTE - FUNCOES ESTADO,
;FUNCOES AUXILIARES SUPORTE - FUNCOES TABULEIRO, FUNCOES AUXILIARES SUPORTE - FUNCOES DESENHO 
;and FUNCOES AUXILIARES SUPORTE - FUNCOES PROBLEMA.
;
;		 - FUNCOES AUXILIARES SUPORTE - FUNCOES ESTADO -  all functions that handle ESTADO structure are
;;; implemented in this subsection.
;
;		- FUNCOES AUXILIARES SUPORTE - FUNCOES TABULEIRO - all functions that handle TABULEIRO 
; array are implemented in this subsection.
;
;		- FUNCOES AUXILIARES SUPORTE - FUNCOES DESENHO - all functions that print structures and 
; enhance outputs are implemented in this subsection.
;
;		- FUNCOES AUXILIARES SUPORTE - FUNCOES PROBLEMA - all functions needed by 
; resolve-problema (function that invokes the supporting material's function procura, where the
; several searches are implemented)
; 		
;		- FUNCAO RESOLVE-PROBLEMA - main function that solves a given board (tabuleiro) with a 
;given strategy -'a* or 'profundidade (DFS) and returns the path of boards since the root board to the goal board.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;
;;;IMPORT DE FICHEIROS AUXILIARES - imports supporting material.
;;;
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

	
;(load "C:\\onlyascii\\PPlan proj indiv\\search-and-planing-individual-project\\procura.lisp")
(in-package :user)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;
;;;DADOS AUXILIARES
;;;
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defparameter *tamanho-linhas-tabuleiro* 4)

(defparameter *tamanho-colunas-tabuleiro* 4)

(defparameter *tabuleiro-final* (make-array '(4 4)
                                            :initial-contents '((1 2 3 4)
                                                                (5 6 7 8)
                                                                (9 10 11 12)
                                                                (13 14 15 nil))))

																
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;
;;;ESTRUTURAS 
;;;
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defstruct estado tabuleiro custo espaco-vazio-linha espaco-vazio-coluna)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;
;;;FUNCOES
;;;
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;FUNCOES AUXILIARES SUPORTE - FUNCOES ESTADO
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun cria-estado (tabuleiro 
                    custo 
                    espaco-vazio-linha 
                    espaco-vazio-coluna)
  (let((copia-do-tabuleiro (copy-array tabuleiro))) 
    (progn (cond( (posicao-valida-p espaco-vazio-linha 
                                    espaco-vazio-coluna) 
                  (return-from cria-estado (make-estado :tabuleiro copia-do-tabuleiro
                                                        :custo custo
                                                        :espaco-vazio-linha espaco-vazio-linha
                                                        :espaco-vazio-coluna espaco-vazio-coluna)))
                ( t 
                  (loop for l from 0 to 3 do 
                        (loop for c from 0 to 3 do  
                              (if(equal (aref copia-do-tabuleiro l c) nil)
                                  (progn 
                                    (setf espaco-vazio-linha  l)
                                    (setf espaco-vazio-coluna c)
                                    (return-from cria-estado (make-estado :tabuleiro copia-do-tabuleiro 
                                                                          :custo custo
                                                                          :espaco-vazio-linha espaco-vazio-linha
                                                                          :espaco-vazio-coluna espaco-vazio-coluna)))))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;FUNCOES AUXILIARES SUPORTE - FUNCOES TABULEIRO
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun posicao-valida-p (linha 
                         coluna)
  ;verifica se a linha e coluna sao validas no tabuleiro
  (if (and (not (or (null linha) 
                    (null coluna))) 
           (< linha *tamanho-linhas-tabuleiro*)
           (< coluna *tamanho-colunas-tabuleiro*)
           (>= coluna 0)
           (>= linha 0))
      (return-from posicao-valida-p T))
  nil)

(defun descobre-linha-tabuleiro-final (val)
  (multiple-value-bind (q r) 
      (floor(/ (- val 
                  1) 
               4))
    (declare (ignore r))
    q))
	
(defun descobre-coluna-tabuleiro-final (val)
  (- (- val 
        1) 
     (* (descobre-linha-tabuleiro-final val) 
        4)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;FUNCOES AUXILIARES SUPORTE - FUNCOES DESENHO
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
(defun desenha-tabuleiro (tab)
  (loop for l from 0 to 3 do
        (let((linha '()))
          (loop for c from 0 to 3 do
                (setf linha 
                      (cons (aref tab 
                                  l 
                                  c) 
                            linha)))
          (format t "~A" (reverse linha))
          (format t "~%"))))

(defun desenha-estado (estado)
  (progn 
    (format t "~%")
    (format t "~A" 'tabuleiro)
    (format t "~%")
    (desenha-tabuleiro (estado-tabuleiro estado))
    (format t "~%")
    (format t "~A" '(custo ))
    (format t "~A" (estado-custo estado))
    (format t "~%")
    (format t "~A" '(posicao-vazia ))
    (format t "(~A.~A)" (estado-espaco-vazio-linha estado) (estado-espaco-vazio-coluna estado))
    (format t "~%")))

(defun corre-e-desenha-resultado-resolve-problema (tabuleiro-inicial 
                                                   estrategia
                                                   ;&key (profundidade-maxima most-positive-fixnum)
                                                   ;(espaco-em-arvore? nil)
                                                   )
  (let ((solucao (resolve-problema tabuleiro-inicial 
                                   estrategia 
                                  ; :profundidade-maxima profundidade-maxima
                                  ; :espaco-em-arvore? espaco-em-arvore?
                                   )))
    (format t "~%")
    (format t "~A" '(Travessia))
    (format t "~%")
    (let((num-estado 0))
		(dolist (tab solucao)
        (format t "~A" '(ESTADO1))
        (format t "~A" num-estado)
        (format t "~%")
        (setf num-estado (+ num-estado 1))
        (desenha-tabuleiro tab)))))
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;FUNCOES AUXILIARES SUPORTE - FUNCOES PROBLEMA
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;		

(defun formula-problema (tabuleiro-inicial)
  (let*((estado-inicial (cria-estado tabuleiro-inicial 
                                     0
                                     nil
                                     nil)))
    (make-problema  :estado-inicial estado-inicial  
                    :operadores (list #'cima #'baixo #'esquerda #'direita)
                    :objectivo? #'objectivo-p
                    :custo (always 1)
                    :heuristica #'manhattan-distance 
                    :hash #'sxhash ;hash
                    :estado= #'equalp)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;HEURISTICA
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

(defun manhattan-distance (estado)
  (let ((h 0))
    (loop for l from 0 to 3 do 
          (loop for c from 0 to 3 do
                (let ((peca (aref (estado-tabuleiro estado)
                                  l
                                  c))
                      (manhattan-value 0))                                  
                  (if (not (null peca))
                      (setf manhattan-value
                            (+ (abs (- l 
                                       (descobre-linha-tabuleiro-final peca)))
                               (abs (- c
                                       (descobre-coluna-tabuleiro-final peca))))))
                  (setf h (+ h manhattan-value))
                  ;(format t "(~A.~A)" peca manhattan-value)
                  ;(format t "~%")
                  )))
    h))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;OBJECTIVO
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

(defun objectivo-p (estado) 
  (equalp *tabuleiro-final* 
          (estado-tabuleiro estado)))
		  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;;OPERADORES					
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun mexe-peca ( novo-tabuleiro 
                   novo-custo 
                   antigo-espaco-vazio-linha 
                   antigo-espaco-vazio-coluna 
                   novo-espaco-vazio-linha 
                   novo-espaco-vazio-coluna 
                   peca-mexida)
  (cond ( (posicao-valida-p novo-espaco-vazio-linha 
                            novo-espaco-vazio-coluna)
          (setf peca-mexida (aref novo-tabuleiro
                                  novo-espaco-vazio-linha
                                  novo-espaco-vazio-coluna))
          (setf (aref novo-tabuleiro
                      antigo-espaco-vazio-linha
                      antigo-espaco-vazio-coluna) 
                peca-mexida)
          (setf (aref novo-tabuleiro
                      novo-espaco-vazio-linha
                      novo-espaco-vazio-coluna)
                nil)
          (list ( cria-estado 
                  novo-tabuleiro 
                  novo-custo novo-espaco-vazio-linha 
                  novo-espaco-vazio-coluna)))
        (t ())))		

(defun cima (estado)
  (let*((novo-tabuleiro (copy-array (estado-tabuleiro estado)))
        (novo-custo (+ (estado-custo estado) 
                       1))
        (antigo-espaco-vazio-linha (estado-espaco-vazio-linha estado))
        (antigo-espaco-vazio-coluna (estado-espaco-vazio-coluna estado)) 
        (novo-espaco-vazio-linha (+ antigo-espaco-vazio-linha 
                                    1))
        (novo-espaco-vazio-coluna antigo-espaco-vazio-coluna)
        (peca-mexida nil))
    (mexe-peca novo-tabuleiro 
               novo-custo 
               antigo-espaco-vazio-linha 
               antigo-espaco-vazio-coluna 
               novo-espaco-vazio-linha 
               novo-espaco-vazio-coluna 
               peca-mexida)))

(defun baixo (estado)
  (let*((novo-tabuleiro (copy-array (estado-tabuleiro estado)))
        (novo-custo (+ (estado-custo estado) 1))
        (antigo-espaco-vazio-linha (estado-espaco-vazio-linha estado))
        (antigo-espaco-vazio-coluna (estado-espaco-vazio-coluna estado))
        (novo-espaco-vazio-linha (- antigo-espaco-vazio-linha 1))
        (novo-espaco-vazio-coluna antigo-espaco-vazio-coluna)
        (peca-mexida nil))
    (mexe-peca novo-tabuleiro 
               novo-custo 
               antigo-espaco-vazio-linha 
               antigo-espaco-vazio-coluna 
               novo-espaco-vazio-linha 
               novo-espaco-vazio-coluna 
               peca-mexida)))

(defun direita (estado)
  (let*((novo-tabuleiro (copy-array (estado-tabuleiro estado)))
        (novo-custo (+ (estado-custo estado) 1))
        (antigo-espaco-vazio-linha (estado-espaco-vazio-linha estado))
        (antigo-espaco-vazio-coluna (estado-espaco-vazio-coluna estado))
        (novo-espaco-vazio-linha antigo-espaco-vazio-linha)
        (novo-espaco-vazio-coluna (- antigo-espaco-vazio-coluna 1)) 
        (peca-mexida nil))
    (mexe-peca novo-tabuleiro 
               novo-custo 
               antigo-espaco-vazio-linha 
               antigo-espaco-vazio-coluna 
               novo-espaco-vazio-linha 
               novo-espaco-vazio-coluna 
               peca-mexida)))

(defun esquerda (estado)
  (let*((novo-tabuleiro (copy-array (estado-tabuleiro estado)))
        (novo-custo (+ (estado-custo estado) 1))
        (antigo-espaco-vazio-linha (estado-espaco-vazio-linha estado))
        (antigo-espaco-vazio-coluna (estado-espaco-vazio-coluna estado))
        (novo-espaco-vazio-linha antigo-espaco-vazio-linha)
        (novo-espaco-vazio-coluna (+ antigo-espaco-vazio-coluna 1)) 
        (peca-mexida nil))
    (mexe-peca novo-tabuleiro 
               novo-custo 
               antigo-espaco-vazio-linha 
               antigo-espaco-vazio-coluna 
               novo-espaco-vazio-linha 
               novo-espaco-vazio-coluna 
               peca-mexida)))
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;;;
;;;FUNCAO RESOLVE-PROBLEMA
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;		

;funcao que resolve o puzzle -argumento tabuleiro-inicial- com a procura especificada no argumento estrategia.
;Na sua invocao, 
	;1- criado o problema com os estado inicial fornecido
	;2- resolvido problema criado com a estrategia de procura escolhida
;retorno:
	;insucesso: NIL
	;sucesso: lista com seq de estados desde os estado inicial ao estado objectivo

(defun resolve-problema (tabuleiro-inicial 
                         estrategia
                         ;&key (profundidade-maxima most-positive-fixnum)
                         ;(espaco-em-arvore? nil)
						 )
  (let*((problema (formula-problema tabuleiro-inicial))
        (solucao (procura problema 
                          estrategia 
                          :profundidade-maxima 16
                          ;:espaco-em-arvore? espaco-em-arvore?
						  )))
    ;(format t "~A" solucao)
    (cond ((or (null solucao) 
               (null (car solucao))) nil)
          (t (mapcar #'(lambda(x)(estado-tabuleiro x)) (car solucao))))))